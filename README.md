myAVR MK2 example: Matrix
=========================

example program for the Switch Matrix

Copyright (c) 2016 Martin Singer <martin.singer@web.de>


Requires
--------

* avr-gcc
* avrdude


Configuration
-------------

* mySmartUSB MK2 programmer
* myAVR MK2 board
* myAVR LCD V2.5 Add-On board
* Switch matrix board (selfmade)

<http://www.myavr.com>


About
-----

* Switch Matrix (SMX):
  5x5 keys

* Dot Matrix LCD:
  2 lines, 16 columns, a 5x7 pixel

* Controller:
  AVR ATmega8L, 3.6864 MHz

* Function:
  - Polls all keys for chainging their state
    (no debounce function)
  - Init driver port as output and set all pins HIGH
  - Init reader port as  input and set all pull-up resistors
  - For polling a key, set the driver row LOW
    and check the reader column for LOW signal.

* Circuit:
  - LCD
      + PortD.2 := LCD RS  (reset)
      + PortD.3 := LCD E   (enable)
      + PortD.4 := LCD DB4 (databit 4 of LCD)
      + PortD.5 := LCD DB5 (databit 5 of LCD)
      + PortD.6 := LCD DB6 (databit 6 of LCD)
      + PortD.7 := LCD DB7 (databit 7 of LCD)

    - SMX (Switch Matrix)
      + PortB.0 := SMX D0 (driver, rows, blue, -)
      + PortB.1 := SMX D1
      + PortB.2 := SMX D2
      + PortB.3 := SMX D3
      + PortB.4 := SMX D4

      + PortC.0 := SMX R0 (reader, cols, white, +)
      + PortC.1 := SMX R1
      + PortC.2 := SMX R2
      + PortC.3 := SMX R3
      + PortC.4 := SMX R4


Problems
--------

* Compiler optimazion
  - If the compiler optimazion is -O1, the switch(0,0) appears as (1,0)
    and the switch (4,0) appears as (0,0).
  - Marking variables as 'volatile' and adding 'asm("");' doesn't help!
  - If the compiler optimizion is -O0, everything works right

