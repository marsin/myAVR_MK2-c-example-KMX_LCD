/**********************************************************************
 myAVR MK2 example: Matrix - example program for the Switch Matrix
 Copyright (c) 2016 Martin Singer <martin.singer@web.de>
 **********************************************************************/

#ifndef MYAVR_MK2__SWITCH_MATRIX__LCD_H
#define MYAVR_MK2__SWITCH_MATRIX__LCD_H

#include "def.h"

// Prototypes
void lcd_init    (void);
void lcd_command (uint8_t);
void lcd_data    (uint8_t);
void lcd_text    (char *);
void lcd_pos     (uint8_t, uint8_t);


#endif // MYAVR_MK2__SWITCH_MATRIX__LCD_H

