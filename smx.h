/**********************************************************************
 myAVR MK2 example: SMX, LCD
   - example program for Switch Matrix (SMX) input and LCD output
 Copyright (c) 2016 Martin Singer <martin.singer@web.de>
 **********************************************************************/

#ifndef MYAVR_MK2__SMX_LCD__SMX_H
#define MYAVR_MK2__SMX_LCD__SMX_H

#include "def.h"

// Constants
#define FIFO_SIZE 8 // n key roll over

// Define: Switch Matrix size and states
#define SW_ROWS 5
#define SW_COLS 5

#define COND_UP   0 // key condition is released
#define COND_DOWN 1 // key condition is pressed

#define EDGE_NONE 0 // key do not change its condition
#define EDGE_FALL 1 // key changes condtiton from released (up) to pressed (down)
#define EDGE_RISE 2 // key changes condtiton from pressed (down) to released (up)

//#define DEBOUNCE 5     // the debounce ISR conts it to 0


// Typedef: Scancode FIFO
typedef struct {
	uint8_t row;
	uint8_t col;
	uint8_t edge;
} fifo_scancode_t;


// Flags
extern volatile uint8_t flag_poll; // flags it is time to poll all the switches
extern volatile uint8_t flag_fifo; // flags there are new states in the fifo

// Variables
extern volatile fifo_scancode_t fifo_scancode[FIFO_SIZE];


// Prototypes
extern void init_smx (void);
extern void poll_smx (void);


#endif // MYAVR_MK2__SMX_LCD__SMX_H

