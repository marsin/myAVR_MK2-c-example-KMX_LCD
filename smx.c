/**********************************************************************
 myAVR MK2 example: SMX, LCD
   - example program for Switch Matrix (SMX) input and LCD output
 Copyright (c) 2016 Martin Singer <martin.singer@web.de>
 **********************************************************************/

#include "smx.h"
//#include "lcd.h"

// Global Flags
volatile uint8_t flag_poll;
volatile uint8_t flag_fifo;

// Global Variables: Scancode FIFO
volatile fifo_scancode_t fifo_scancode[FIFO_SIZE] = {{ 0, 0, EDGE_NONE }};

// Private Prototypes
//void    poll (void);
void    push_scancode (const uint8_t, const uint8_t, const uint8_t);
uint8_t get_edge (const uint8_t, const uint8_t, volatile const uint8_t);
uint8_t get_cond (const uint8_t, const uint8_t);


/** ISR: Poll SMX
 * For F_CPU is 3686400ul and the prescaler for the ISR is 1024
 * 3686400 Hz / 1024 = 3600 Hz
 * 3600 Hz / 25 1/s = 144 (polling the switch matrix 25 times per second)
 */
ISR (TIMER0_OVF_vect)
{
	static uint8_t c = 0;

	if (144 != c++)
	{
		flag_poll = 1; // !0 flags it's time to poll the switches
		c = 0;
	}
}


/** Public Function: Initialize Module - Switch Matrix
 *
 */
void init_smx (void)
{
	uint8_t
		row = 0,
		col = 0;

	/* Flags
	 */
	flag_poll = 0;
	flag_fifo = 0;

	/* Init Scancode FIFO
	 * only a pos of the FIFO buffer is empty (edge == EDGE_NONE) it becomes filled
	 * if a buffer cell is read it becomes cleared, by inserting EDGE_NONE
	 */
	for (row=0; row<SW_ROWS; ++row)
	{
		for (col=0; col<SW_COLS; ++col)
		{
			fifo_scancode[col].col  = 0;
			fifo_scancode[col].row  = 0;
			fifo_scancode[col].edge = EDGE_NONE;
		}
	}

	/* Init Row as Driver
	 * set DDR registers as output (DDR: DataDirectionRegister)
	 * set PORT registers to HIGH (initial)
	 */
	SMX_DRIVER_DDR  |=  ((1<<SMX_D0) | (1<<SMX_D1) | (1<<SMX_D2) | (1<<SMX_D3) | (1<<SMX_D4));
	SMX_DRIVER_PORT |=  ((1<<SMX_D0) | (1<<SMX_D1) | (1<<SMX_D2) | (1<<SMX_D3) | (1<<SMX_D4));

	/* Init Column as Reader
	 * set DDR registers as output
	 * set PORT pull-up resistors
	 * the PIN registers are the input
	 */
	SMX_READER_DDR  &= ~((1<<SMX_R0) | (1<<SMX_R1) | (1<<SMX_R2) | (1<<SMX_R3) | (1<<SMX_R4));
	SMX_READER_PORT |=  ((1<<SMX_R0) | (1<<SMX_R1) | (1<<SMX_R2) | (1<<SMX_R3) | (1<<SMX_R4));

	/* Init TIMER0
	 * set prescaler to 8       (TCCR0  = xxxx.x010)
	 * set prescaler to 1024    (TCCR0  = xxxx.x101)
	 * allow overflow interrupt (TIMSK |= 0000.0001)
	 * enable interrupts in main init function!
	 */
//	TCCR0  = (1<<CS01);
	TCCR0  = ((1<<CS00) | (1<<CS02));
	TIMSK |= (1<<TOIE0);
//	TIMSK |= ((1<<TOIE1) | (1<<TOIE0));

	return;
}

/** Public Function: Poll Switches
 *
 */
void poll_smx (void)
{
	uint8_t
		cond = COND_UP,
		edge = EDGE_NONE,
		row  = 0,
		col  = 0;

	for (row=0; row<SW_ROWS; ++row)
	{
		for (col=0; col<SW_COLS; ++col)
		{
			cond = get_cond(row, col);
			edge = get_edge(row, col, cond);

			if (EDGE_RISE == edge || EDGE_FALL == edge)
			{
				push_scancode(row, col, edge);
			}
		}
	}

	return;
}

/** Push Scan Code to FIFO
 *
 */
void push_scancode (
	const uint8_t row,
	const uint8_t col,
	const uint8_t edge)
{
//	volatile static uint8_t pos_fifo = 0;
	static uint8_t pos_fifo = 0;

	/* only if the buffer on position is free, write into it
	 * buffer becomes cleared from a transmission function
	 */
	if (EDGE_NONE == fifo_scancode[pos_fifo].edge)
	{
		// write scancode into fifo buffer
		fifo_scancode[pos_fifo].row  = (uint8_t) row;
		fifo_scancode[pos_fifo].col  = (uint8_t) col;
		fifo_scancode[pos_fifo].edge = (uint8_t) edge;
		flag_fifo = 1; // !0 signals something happened

		// set next fifo pos
		if (++pos_fifo >= FIFO_SIZE)
		{
			pos_fifo = 0;
		}
	}

	return;
}

/** Private Function: Get Edge
 *
 */
uint8_t get_edge (
	const uint8_t row,
	const uint8_t col,
	const uint8_t cond)
{
	static uint8_t
		table_cond[SW_ROWS][SW_COLS] = { COND_UP }; // Key condtion table
	uint8_t
		edge = EDGE_NONE;

	if (cond != table_cond[row][col])
	{
		switch (cond)
		{
			case COND_UP:
				edge = EDGE_RISE;
				break;

			case COND_DOWN:
				edge = EDGE_FALL;
				break;

			default:
				/* Error handling:
				 *   - return default value
				 *   - do not save condition (implizit)
				 */
				return EDGE_NONE;
				break;
		}

		table_cond[row][col] = cond; // save condition
	}

	return edge;
}

/** Private Function: Get Condtition
 *
 */
uint8_t get_cond (
	const uint8_t row,
	const uint8_t col)
{
	uint8_t
		cond = COND_UP;
	volatile uint8_t
		pin_reader = 0xFF;

//	SMX_DRIVER_PORT = ~(1<<row);
	SMX_DRIVER_PORT &= ~((1<<row) & 0x1F); // 0001 1111 marks the upper 3 pins out

	pin_reader  = ~SMX_READER_PIN;
	pin_reader &= 0x1F;                    // 0001 1111 masks the upper 3 pins out (not connected)

	if (0 != (pin_reader & (1<<col))) {
		cond = COND_DOWN;
	}

	SMX_DRIVER_PORT |=  ((1<<SMX_D0) | (1<<SMX_D1) | (1<<SMX_D2) | (1<<SMX_D3) | (1<<SMX_D4));
	return cond;
}

