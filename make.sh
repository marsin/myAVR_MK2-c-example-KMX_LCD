#!/bin/bash
# myAVR MK2 example: SMX, LCD
#   - example program for Switch Matrix (SMX) input and LCD output
# Copyright (c) 2016 Martin Singer <martin.singer@web.de>

<< EOF_OPTIMIZE
avr-gcc smx.c lcd.c main.c -mmcu=atmega8 -O1 -Wall -o fw.elf && \
	avr-objcopy -O ihex fw.elf fw.hex && \
	avrdude -p m8 -c avr911 -P /dev/ttyUSB0 -U flash:w:fw.hex:i
EOF_OPTIMIZE

#<< EOF_NO-OPTIMIZATION
avr-gcc smx.c lcd.c main.c -mmcu=atmega8 -O0 -Wall -o fw.elf && \
	avr-objcopy -O ihex fw.elf fw.hex && \
	avrdude -p m8 -c avr911 -P /dev/ttyUSB0 -U flash:w:fw.hex:i
#EOF_NO-OPTIMIZATION

<< EOF_COMPILE-ONLY
avr-gcc smx.c lcd.c main.c -mmcu=atmega8 -O0 -Wall -o fw.elf && \
	avr-objcopy -O ihex fw.elf fw.hex
EOF_COMPILE-ONLY

